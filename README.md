# Electricity Demand Forecasting based on Smart Meter Data
Based on the Kaggle dataset (https://www.kaggle.com/jeanmidev/smart-meters-in-london), the goal of this study is to compare several well known machine learning algorithms on the task of time series forecasting. A report is also provided showing the results of the comparison.

The studied algorithms are:
* SARIMA
* RANDOM FOREST
* LINEAR REGRESSION
* DECISION TREES
* SUPPORT VECTOR MACHINES
* MULTILAYER PERCEPTRON
